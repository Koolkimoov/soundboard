# soundBoard

## Description
This is an open source project for my own satisfaction and because I did not want to buy a Streamdeck or VoiceMod.  

## Installation
The compiled project is in the `out/artifacts/soundBoard_jar/` directory you can use it as is. If you move it to another directory, you must move the `settings.txt` and `device.txt` files. You need [openjdk-17](https://download.oracle.com/java/17/archive/jdk-17.0.3.1_windows-x64_bin.exe) to run the project. You can compile it with the same jdk.
 
## Contributing
Contributions are always appreciated, feel free to branch or contribute directly.

## License
This project is build using Java and the [jNativeHook](https://github.com/kwhat/jnativehook) project.

## Project status
The feature set is finished and this project is not being worked on at the moment.
