import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import java.io.*;
import java.util.ArrayList;
import java.util.Objects;

/*
 * settings manager class that manages settings
 * */

public class settingsManager {

    // save sound entries in a given file
    // save structure is :
    //  file path; modifier; key\n
    public static void save(ArrayList<soundEntry> soundList, File file) {
        try {
            FileWriter writer = new FileWriter(file);
            for (soundEntry sound : soundList) {
                writer.write(sound.getFile().toString() + " ;");
                writer.write(sound.getModifier() + " ;");
                writer.write(sound.getKey() + " ;");
                writer.write(sound.getVolume() + " \n");
            }
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // save string in a given file
    public static void save(String string, File file) {
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(string);
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // read saved audio device
    public static Mixer.Info readDevice(File readFile) {
        Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
        BufferedReader reader = null;
        // if saved audio device is available, return that device
        try {
            reader = new BufferedReader(new FileReader(readFile));
        } catch (IOException e) {
            try {
                if (readFile.createNewFile())
                    reader = new BufferedReader(new FileReader(readFile));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        try {
            String line = reader.readLine();
            if (line != null) {
                for (Mixer.Info info : mixerInfo) {
                    if (Objects.equals(info.getName(), line)) {
                        return info;
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        // if saved audio device is not available, return first in list as default
        return mixerInfo[0];
    }

    // read sound list. no need to check if file exists, sound will just not play
    public static ArrayList<soundEntry> readSoundList(File readFile) {
        ArrayList<soundEntry> soundList = new ArrayList<soundEntry>();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(readFile));
        } catch (IOException e) {
            try {
                if (readFile.createNewFile())
                    reader = new BufferedReader(new FileReader(readFile));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        try {
            String line = reader.readLine();
            while (line != null && !line.isEmpty()) {
                // split line (sound entry) with ";" separator
                String[] list = line.split(";");
                File file = new File(list[0].strip());
                String modifier = list[1].strip();
                String key = list[2].strip();
                int volume = Integer.parseInt(list[3].strip());
                // add newly read sound entry
                soundList.add(new soundEntry(file, modifier, key, volume));
                line = reader.readLine();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return soundList;
    }
}
