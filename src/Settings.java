import org.jnativehook.GlobalScreen;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Objects;

/*
 * Settings gui class that allow the user to modify diverse settings
 * */

public class Settings extends JDialog {
    private final JComboBox<String> deviceSelector;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton addSoundButton;
    private JPanel soundPanel;
    private JPanel settingsPanel;
    private JButton removeSoundButton;

    public Settings() {

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        soundPanel.setLayout(new GridLayout(0, 4));
        contentPane.setSize(400, 200);

        // display columns headers for sound list
        addHeads();
        // display sounds currently in sound list
        loadSounds();

        // setup combo box with all available audio outputs
        deviceSelector = new JComboBox<>();
        Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
        for (Mixer.Info info : mixerInfo) {
            if (info.getDescription().contains("Playback")) {
                deviceSelector.addItem(info.getName());
            }
        }
        settingsPanel.add(deviceSelector);

        // set combo box selected as the previously selected device
        for (int i = 0; i < deviceSelector.getItemCount(); i++) {
            if (Objects.equals(deviceSelector.getItemAt(i), mainWindow.device.getName())) {
                deviceSelector.setSelectedIndex(i);
                break;
            }
        }

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        // call addSound function when dedicated button is pressed
        addSoundButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                addSound();
            }
        });

        // create delete sound gui window
        removeSoundButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                deleteSound deletesound = new deleteSound();
                deletesound.pack();
                deletesound.setLocationRelativeTo(null);
                deletesound.setVisible(true);

                // delete all currently displayed sounds
                for (Component c : soundPanel.getComponents()) {
                    soundPanel.remove(c);
                }

                // reload all sounds
                addHeads();
                loadSounds();
            }
        });
    }

    // add header to sound list display structure in settings window
    private void addHeads() {
        JTextField file = new JTextField("File");
        file.setHorizontalAlignment(SwingConstants.CENTER);
        file.setEditable(false);
        soundPanel.add(file);
        JTextField modifier = new JTextField("Modifier");
        modifier.setHorizontalAlignment(SwingConstants.CENTER);
        modifier.setEditable(false);
        soundPanel.add(modifier);
        JTextField keyHead = new JTextField("Key");
        keyHead.setHorizontalAlignment(SwingConstants.CENTER);
        keyHead.setEditable(false);
        soundPanel.add(keyHead);
        JTextField volumeHead = new JTextField("Volume");
        volumeHead.setHorizontalAlignment(SwingConstants.CENTER);
        volumeHead.setEditable(false);
        soundPanel.add(volumeHead);
    }

    // save current settings before closing settings window
    private void onOK() {
        ArrayList<soundEntry> list = mainWindow.soundList;
        list.add(mainWindow.stopBind);
        settingsManager.save(list, mainWindow.soundListFile);
        settingsManager.save(deviceSelector.getSelectedItem().toString(), mainWindow.deviceFile);
        dispose();

    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    // create blank sound entry in the settings window that can be modified
    private void addSound() {

        soundEntry sound = new soundEntry();
        mainWindow.soundList.add(sound);
        loadSounds();

    }

    // load sounds in sound list to the settings window
    private void loadSounds() {

        for (soundEntry sound : mainWindow.soundList) {
            soundPanel.add(sound.getFileChooser());
            soundPanel.add(sound.getModifierChooser());
            soundPanel.add(sound.getKeyChooser());
            soundPanel.add(sound.getVolumeChooser());
        }

        soundPanel.add(mainWindow.stopBind.getFileChooser());
        soundPanel.add(mainWindow.stopBind.getModifierChooser());
        soundPanel.add(mainWindow.stopBind.getKeyChooser());
        soundPanel.add(mainWindow.stopBind.getFiller());

        pack();
        repaint();
    }

}
