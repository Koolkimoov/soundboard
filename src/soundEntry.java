import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyAdapter;
import org.jnativehook.keyboard.NativeKeyEvent;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

/*
 * sound entry class that concatenates several gui elements in a bigger,
 * repeatably usable element in order to display x amount of sounds
 * */
public class soundEntry {

    private File file;
    private String modifier;
    private String key;

    private int volume;

    // composed of a file chooser, a combo box for modifier selection and a key chooser
    private JButton fileChooser;
    private JComboBox modifierChooser;
    private JTextField keyChooser;
    private JSlider volumeChooser;

    private JTextField filler;

    private NativeKeyAdapter listener;

    // constructor for already known file
    public soundEntry(File file, String modifier, String key, int volume) {
        init();
        // set previously chosen file path, displayed text as well as modifier information and chosen key
        this.file = file;
        String[] fileText = this.file.toString().split("\\\\");
        this.fileChooser.setText(fileText[fileText.length - 1]);
        this.modifier = modifier;
        this.modifierChooser.setSelectedItem(this.modifier);
        this.key = key;
        this.keyChooser.setText(this.key);
        this.volume = volume;
        this.volumeChooser.setValue(volume);
    }

    // constructor for blank sound entry
    public soundEntry() {
        init();
    }

    private void init() {
        // set default values for sound file
        file = new File("./");
        fileChooser = new JButton("");
        fileChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                file = onAddFile();
                String[] fileText = file.toString().split("\\\\");
                fileChooser.setText(fileText[fileText.length - 1]);
            }
        });

        // set default values for modifier
        modifier = "";
        modifierChooser = new JComboBox(mainWindow.modifiersOptions);
        modifierChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modifier = modifierChooser.getSelectedItem().toString();
            }
        });

        // set default value for key
        key = "";
        keyChooser = new JTextField(key);
        keyChooser.setEditable(false);
        keyChooser.setHorizontalAlignment(SwingConstants.CENTER);

        // instantiate listener - each sound entry has its own
        // when activated, listen for a single keystroke and store it before deactivating key listener
        listener = new NativeKeyAdapter() {
            public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
                keyChooser.setText(NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode()));
                key = keyChooser.getText();
                GlobalScreen.removeNativeKeyListener(listener);
            }
        };

        // if user click on key chooser text box, listen to keystrokes
        keyChooser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                GlobalScreen.addNativeKeyListener(listener);
            }
        });

        volumeChooser = new JSlider();
        volume = 80;
        volumeChooser.setValue(volume);
        filler = new JTextField();
        filler.setEditable(false);

        volumeChooser.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                volume = volumeChooser.getValue();
            }
        });
    }

    // when file chooser is clicked, instantiate and display file chooser window
    private File onAddFile() {
        JFrame frame = new JFrame();
        JFileChooser select = new JFileChooser("./");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("WAV sound files", "wav");
        select.setFileFilter(filter);
        int res = select.showDialog(frame, "Chose");
        if (res == JFileChooser.APPROVE_OPTION) {
            return select.getSelectedFile();
        }
        return new File("./");
    }

    // setters and getters

    public File getFile() {
        return file;
    }

    public String getModifier() {
        return modifier;
    }

    public String getKey() {
        return key;
    }

    public JButton getFileChooser() {
        return fileChooser;
    }

    public JComboBox getModifierChooser() {
        return modifierChooser;
    }

    public JTextField getKeyChooser() {
        return keyChooser;
    }

    public JSlider getVolumeChooser() {
        return volumeChooser;
    }

    public int getVolume() {
        return volume;
    }

    public JTextField getFiller() {
        return filler;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
        this.modifierChooser.setSelectedItem(modifier);
    }

    public void setKey(String key) {
        this.key = key;
        this.keyChooser.setText(key);
    }
}
