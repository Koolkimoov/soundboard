import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class main {
    public static void main(String[] args) throws NativeHookException {

        // Instanciate main application window
        mainWindow main = new mainWindow();

        // Setup keystroke listener
        GlobalScreen.registerNativeHook();
        GlobalScreen.addNativeKeyListener(main.listener);

        // Get the logger for "org.jnativehook" and set the level to warning.
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.WARNING);

        // Don't forget to disable the parent handlers.
        logger.setUseParentHandlers(false);
    }
}
