import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * Player class that takes care of playing sounds when keys are pressed
 * */
public class playSound {

    List<Clip> playing = new ArrayList<>();

    public void playFile(File file, float gain) {
        // play desired file on de predetermined audio device
        try {
            Clip clip = AudioSystem.getClip(mainWindow.device);
            clip.open(AudioSystem.getAudioInputStream(file));
            FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            float v = gain * (volume.getMaximum() + 40) / 100 - 41;
            volume.setValue(v);
            clip.start();

            // keep track of all sounds
            playing.add(clip);

        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void killAllSounds() {
        // if needed stop all sounds being played
        for (Clip clip : playing) {
            clip.stop();
        }
        playing.clear();
    }
}
