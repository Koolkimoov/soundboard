import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import javax.sound.sampled.Mixer;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

/*
 * main window gui class, only shows settings button and current sound being played
 * */
public class mainWindow {

    static public String[] modifiersOptions = {"", "Ctrl", "Alt"};
    static public ArrayList<soundEntry> soundList = new ArrayList<soundEntry>();
    static public soundEntry stopBind = new soundEntry(new File(""), "Alt", "F24", 0);

    static public Mixer.Info device;
    static public File soundListFile = new File("settings.txt");
    static public File deviceFile = new File("device.txt");
    private final playSound player = new playSound();
    private String currentModifier = "";

    private final JFrame frame = new JFrame("SoundBoard");
    private JPanel mainPanel;
    private JButton currentSound;

    // setup listener with modifiers for more options
    NativeKeyListener listener = new NativeKeyListener() {
        @Override
        public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
        }

        @Override // action performed when key is pressed down
        public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
            String key = NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode());
            if (key == "Alt" || key == "Ctrl") {
                currentModifier = key;
            }
            // kill all sound
            else if (Objects.equals(currentModifier, stopBind.getModifier()) && Objects.equals(key, stopBind.getKey())) {
                player.killAllSounds();
            } else {
                for (soundEntry sound : soundList) {
                    if (Objects.equals(sound.getKey(), key) && Objects.equals(sound.getModifier(), currentModifier)) {
                        player.playFile(sound.getFile(), sound.getVolume());
                        String[] temp = sound.getFile().toString().split("\\\\");
                        currentSound.setText("Current: " + temp[temp.length - 1]);
                    }
                }
            }
        }

        @Override // action performed when key is releaser
        public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
            String key = NativeKeyEvent.getKeyText(nativeKeyEvent.getKeyCode());
            if (key == "Alt" || key == "Ctrl") {
                currentModifier = "";
            }
        }
    };

    public mainWindow() {
        soundList = settingsManager.readSoundList(soundListFile);
        device = settingsManager.readDevice(deviceFile);
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setSize(400, 75);

        if (soundList.size() > 0)
        if (!soundList.get(soundList.size() - 1).getFile().exists()) {
            stopBind.setKey(soundList.get(soundList.size() - 1).getKey());
            stopBind.setModifier(soundList.get(soundList.size() - 1).getModifier());
            soundList.remove(soundList.size() - 1);
        }
        stopBind.getFileChooser().setEnabled(false);
        stopBind.getFileChooser().setText("Stop all sounds");

        currentSound.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // when settings button is pressed, deactivate key listener and instantiate settings window
                super.mouseClicked(e);

                GlobalScreen.removeNativeKeyListener(listener);
                Settings settings = new Settings();
                settings.pack();
                settings.setLocationRelativeTo(null);
                settings.setVisible(true);

                // reactivate key listener
                GlobalScreen.addNativeKeyListener(listener);

                // update settings data
                soundList = settingsManager.readSoundList(soundListFile);
                device = settingsManager.readDevice(deviceFile);

                if (!soundList.get(soundList.size() - 1).getFile().exists()) {
                    stopBind.setKey(soundList.get(soundList.size() - 1).getKey());
                    stopBind.setModifier(soundList.get(soundList.size() - 1).getModifier());
                    soundList.remove(soundList.size() - 1);
                }
            }
        });

    }
}
